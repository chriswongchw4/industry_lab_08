package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a Scanner
        File myfile = new File(fileName + ".csv");
        try (PrintWriter pw = new PrintWriter(new FileWriter(myfile))) {


            for (int i = 0; i < films.length; i++) {
                String name = films[i].getName();
                int year = films[i].getYear();
                int length = films[i].getLengthInMinutes();
                String director = films[i].getDirector();

                pw.println(name + "," + year + "," + length + "," + director);

            }


        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }


    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}

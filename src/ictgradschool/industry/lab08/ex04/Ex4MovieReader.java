package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a PrintWriter
        File myfile = new File(fileName + ".csv");

        ArrayList<Movie> temp = new ArrayList<>();

        try (Scanner scanner = new Scanner(myfile)){

            scanner.useDelimiter(",|\\r\\n");

            while (scanner.hasNext()){
                String name =scanner.next();
                int year = scanner.nextInt();
                int length = scanner.nextInt();
                String director = scanner.next();

                temp.add(new Movie(name,year,length,director));

            }


        }catch (IOException e){
            System.out.println("Error: "+e.getMessage());
        }

        Movie [] films= new Movie[temp.size()];
        for (int x=0;x<temp.size();x++){
            films[x]=temp.get(x);
        }




        return films;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}

package ictgradschool.industry.lab08.ex01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {


        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        File input2 = new File("input2.txt");

        try (FileReader fr = new FileReader(input2)) {


            int readChar;
            while ((readChar = fr.read()) != -1) {

                if (readChar == 'e' || readChar == 'E') {
                    numE++;
                }

                total++;
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
        // TODO Use a FileReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.

        File input2 = new File("input2.txt");

        try (BufferedReader fr = new BufferedReader(new FileReader(input2))){
           int readChar;
            while ((readChar = fr.read()) != -1) {

                if (readChar == 'e' || readChar == 'E') {
                    numE++;
                }

                total++;
            }
        } catch (IOException e) {
            System.out.println("Error: "+e.getMessage());
        }

        // TODO Use a BufferedReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}

package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        File myReaderFile = new File(fileName + ".txt");

        try (BufferedReader reader = new BufferedReader(new FileReader(myReaderFile))) {
            String line = null;
            while ((line = reader.readLine()) != null){
                System.out.println(line);
            }


        } catch (IOException e) {
            System.out.print("Error: " + e.getMessage());
        }


        // TODO Use a BufferedReader.
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
